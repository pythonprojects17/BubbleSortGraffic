import tkinter
import random

window = 1000
canvas = 1000

gui = tkinter.Tk()
gui.geometry(f'{window}x{canvas}')
gui.title('Show')
c = tkinter.Canvas(gui, width=canvas, height=canvas, bg='white')
c.pack()


def draw(array):
    s_s = canvas // len(array)
    c.delete(tkinter.ALL)
    for i, item in enumerate(array):
        coordinates = [
            i * s_s,
            canvas,
            (i + 1) * s_s,
            canvas - item * s_s
        ]

        c.create_rectangle(*coordinates, fill='black')
    gui.update()


def bubble(array):
    tmp = list(array)
    for _ in range(0, len(tmp) - 1):
        for i in range(0, len(tmp) - _ - 1):
            if tmp[i] > tmp[i + 1]:
                tmp[i], tmp[i + 1] = tmp[i + 1], tmp[i]
            draw(tmp)
    return tmp
        

arr = list(range(1, 50))
random.shuffle(arr)
bubble(arr)
